import React, { Component } from "react";
import Moment from "react-moment";

class WeatherDisplay extends Component {
  // Get cardinal direction from degrees
  getDirection = deg => {
    return [
      "north",
      "north-northeast",
      "northeast",
      "east-northeast",
      "east",
      "east-southeast",
      "southeast",
      "south-southeast",
      "south",
      "south-southwest",
      "southwest",
      "west-southwest",
      "west",
      "west-nothwest",
      "nothwest",
      "north-northwest",
      "north"
    ][Math.round((deg / 11.25 / 2) % 360)];
  };

  // Get day or night from time
  getDayOrNight = () => {
    const time = new Date(this.props.data.dt * 1000);
    const hours = time.getHours();

    return hours > 6 && hours < 19 ? "day" : "night";
  };

  render() {
    const { data } = this.props;

    return (
      <div className="container">
        <div className="weather">
          {data.name !== undefined ? (
            <h1 className="weather__title">
              Weather in <span className="title__name">{data.name}</span>,{" "}
              <span className="title__country">{data.sys.country}</span>
            </h1>
          ) : (
            <h1 className="weather__date">
              <Moment unix format="DD. MM. - HH:mm">
                {data.dt}
              </Moment>
            </h1>
          )}

          <i
            className={`wi wi-owm-${this.getDayOrNight()}-${
              data.weather[0].id
            } weather__icon`}
          />

          <h2 className="weather__desc">{data.weather[0].description}</h2>
          <h2 className="weather__temp">{data.main.temp} °C</h2>
          
        </div>

        <table className="weather__table">
          <tbody>
            <tr>
              <td className="table__label">Humidity:</td>
              <td className="table__data">{data.main.humidity}%</td>
            </tr>
            <tr>
              <td className="table__label">Pressure:</td>
              <td className="table__data">{data.main.pressure} hPa</td>
            </tr>
            <tr>
              <td className="table__label">Clouds:</td>
              <td className="table__data">{data.clouds.all}%</td>
            </tr>
            <tr>
              <td className="table__label">Wind Speed:</td>
              <td className="table__data">{data.wind.speed} m/s</td>
            </tr>
            <tr>
              <td className="table__label">Wind Direction:</td>
              <td className="table__data">
                {this.getDirection(data.wind.deg)}, {Math.floor(data.wind.deg)}°
              </td>
            </tr>
            {data.sys.sunrise ? (
              <React.Fragment>
                <tr>
                  <td className="table__label">Sunrise:</td>
                  <td className="table__data">
                    <Moment unix format="HH:mm">
                      {data.sys.sunrise}
                    </Moment>
                  </td>
                </tr>
                <tr>
                  <td className="table__label">Sunset:</td>
                  <td className="table__data">
                    <Moment unix format="HH:mm">
                      {data.sys.sunset}
                    </Moment>
                  </td>
                </tr>
              </React.Fragment>
            ) : null}
          </tbody>
        </table>
      </div>
    );
  }
}

export default WeatherDisplay;
