import React, { Component } from "react";
import axios from "axios";

import WeatherDisplay from "./WeatherDisplay";
import Loader from "../layout/Loader";

export class Weather extends Component {
  state = {
    data: {},
    path: "weather"
  };

  // Get current weather based on latitude and longtitude from API
  // Default values -> London
  getWeather = (lat, lon) => {
    axios
      .get(
        `https://api.openweathermap.org/data/2.5/${
          this.state.path
        }?lat=${lat}&lon=${lon}&appid=acb1d8952af055d0a2443dbc493aa5fa&units=metric`
      )
      .then(res => {
        // Set data we get from API to state
        this.setState({ data: res.data });
      })
      .catch(console.log);
  };

  getLocation = () => {
    const location = window.navigator.geolocation;
    // Get user current location from browser
    if (location) {
      location.getCurrentPosition(
        position => {
          this.getWeather(position.coords.latitude, position.coords.longitude);
        },
        error => {
          console.log(error + " Error getting weather data!");
        }
      );
    }
  };

  updatePath = path => {
    if (path === "/") {
      this.setState({ path: "weather" });
    } else if (path === "/forecast") {
      this.setState({ path: "forecast" });
    }
  };

  componentDidMount() {
    this.updatePath(this.props.match.path);
    this.getLocation();
    this.getWeather();
  }

  render() {
    const { data } = this.state;

    switch (this.state.path) {
      case "weather":
        return (
          <section className="content flex">
            {data.main !== undefined ? (
              <WeatherDisplay data={data} />
            ) : (
              <Loader />
            )}
          </section>
        );
      case "forecast":
        return (
          <section className="content grid">
            {data.list !== undefined && data.list !== 0 ? (
              data.list.map(item => {
                return <WeatherDisplay key={item.dt} data={item} />;
              })
            ) : (
              <Loader />
            )}
          </section>
        );

      default:
        return <Loader />;
    }
  }
}

export default Weather;
