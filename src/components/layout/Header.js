import React from "react";
import { Link } from "react-router-dom";

const Header = () => {
  return (
    <header className="header">
      <nav className="navbar">
        <Link to="/" className="navbar__link">
          Current
        </Link>
        <Link to="/forecast" className="navbar__link">
          Forecast
        </Link>
      </nav>
    </header>
  );
};

export default Header;
