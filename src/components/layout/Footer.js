import React from "react";

const Footer = () => {
  return (
    <footer className="footer">
      <p>
        2018 - 2019 &copy;{" "}
        <a
          href="https://karel-houf.gitlab.io/"
          className="footer__link"
          target="_blank"
          rel="noopener noreferrer"
        >
          Karel-František Houf
        </a>
      </p>
    </footer>
  );
};

export default Footer;
