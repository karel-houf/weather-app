import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Header from "./components/layout/Header";
import Footer from "./components/layout/Footer";
import Weather from "./components/Weather/Weather";

const App = () => {
  return (
    <Router>
        <React.Fragment>
          <Header />

          <Route exact path="/" component={Weather} />
          <Route path="/forecast" component={Weather} />

          <Footer />
        </React.Fragment>
    </Router>
  );
};

export default App;
